﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using AlisProject.Models;
using Parse;

namespace AlisProject.Controllers
{
    public class DriversController : Controller
    {
        // GET: Drivers
        public async System.Threading.Tasks.Task<ActionResult> Index()
        {
            try
            {
                var x = (await ParseUser.Query.Include("car").FindAsync()).ToList();
                List<Driver> d = new List<Driver>();
                foreach (var y in x)
                {
                    d.Add(new Driver(y));
                }
                return View(d);
            }
            catch
            {
                int yu = 0;
            }
            return View(new List<Driver>());
        }

        // GET: Drivers/Details/5
        public async System.Threading.Tasks.Task<ActionResult> Details(String id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var driver = (await ParseUser.Query.WhereEqualTo("objectId", id).FirstAsync());
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(new Driver(driver));
        }

        // GET: Drivers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Drivers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> Create(Driver driver)
        {
            if (ModelState.IsValid)
            {
                driver.Password = driver.VPassword;
                await driver.SignUpAsync();
                return RedirectToAction("Index");
            }

            return View(driver);
        }

        // GET: Drivers/Edit/5
        public async System.Threading.Tasks.Task<ActionResult> Edit(String id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var driver = (await ParseUser.Query.WhereEqualTo("objectId", id).FirstAsync());
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(new Driver(driver));
        }

        // POST: Drivers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> Edit( Driver driver)
        {
            if (ModelState.IsValid)
            {
                var driver1 = new Driver((await ParseUser.Query.WhereEqualTo("objectId", driver.ObjectId).FirstAsync()));
                
                await ParseUser.LogInAsync(driver1.Username, driver1.VPassword);
                driver.Password = driver.VPassword;
                await driver.getUse().SaveAsync();
                try
                {
                    ParseUser.LogOut();
                }
                catch { }
                return RedirectToAction("Index");

            }
            return View(driver);
        }

        // GET: Drivers/Delete/5
        public async System.Threading.Tasks.Task<ActionResult> Delete(String id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var driver = new Driver((await ParseUser.Query.WhereEqualTo("objectId", id).FirstAsync()));
            if (driver == null)
            {
                return HttpNotFound();
            }
            await ParseUser.LogInAsync(driver.Username, driver.VPassword);
            driver.Password = driver.VPassword;
            await ParseUser.CurrentUser.DeleteAsync();
            try
            {
                ParseUser.LogOut();
            }
            catch(Exception ex)
            {
                int op = 0;
            }
            return View(driver);
        }
    }
}
