﻿using AlisProject.Models;
using Parse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AlisProject
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            
            ParseObject.RegisterSubclass<Car>();
            ParseObject.RegisterSubclass<Shipment>();
            ParseObject.RegisterSubclass<Driver>();
            ParseObject.RegisterSubclass<ShipmentLocations>();

            ParseClient.Initialize(new ParseClient.Configuration
            {
                ApplicationId = "zyjbpFVCKeZRsaKzIRjLWBEFsCLpRTfh0uzbAbt1",
                WindowsKey = "F6kSQ8dDe8Ib8Xe6Eo6q3EcT93EJNTAmiwOYMbaC",
                
                Server = "https://parseapi.back4app.com/"
            });
        }
    }
}
