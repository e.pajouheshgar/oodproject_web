﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlisProject.ViewModels
{
    public class ShipmentLocationsDetail
    {
        public List<Models.ShipmentLocations> data { get; set; }
        public Models.Shipment shipment { get; set; }
    }
}