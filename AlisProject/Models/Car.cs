﻿using Parse;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AlisProject.Models
{
    [ParseClassName("Car")]
    public class Car : ParseObject
    {
        [Display(Name = "شماره پلاک")]
        [ParseFieldName("License_Plate")]
        public String License_Plate
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }

        [Display(Name ="نام")]
        [ParseFieldName("Name")]
        public String Name
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [Display(Name = "شماره شاسی")]

        [ParseFieldName("ChassisNumber")]
        public String ChassisNumber
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [Display(Name = "توضیحات")]
        [ParseFieldName("Description")]
        public String Description
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
    }
}