﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Parse;
namespace AlisProject.Models
{
    [ParseClassName("ShipmentLocations")]
    public class ShipmentLocations : ParseObject
    {
        [ParseFieldName("Location")]
        public string Location
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [ParseFieldName("LocationTime")]
        public DateTime LocationTime
        {
            get { return GetProperty<DateTime>(); }
            set { SetProperty<DateTime>(value); }
        }
        [ParseFieldName("Driver")]
        public Driver Driver
        {
            get { return GetProperty<Driver>(); }
            set { SetProperty<Driver>(value); }

        }
        [ParseFieldName("Shipment")]
        public Shipment Shipment
        {
            get { return GetProperty<Shipment>(); }
            set { SetProperty<Shipment>(value); }

        }
    }
}