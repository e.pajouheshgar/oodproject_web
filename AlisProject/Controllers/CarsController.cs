﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AlisProject.Models;
using Parse;

namespace AlisProject.Controllers
{
    public class CarsController : Controller
    {
        // GET: Cars
        public async System.Threading.Tasks.Task<ActionResult> Index()
        {
            return View((await new ParseQuery<Car>().FindAsync()).ToList());
        }

        // GET: Cars/Details/5
        public async System.Threading.Tasks.Task<ActionResult> Details(String id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = (await new ParseQuery<Car>().WhereEqualTo("objectId", id).FirstAsync());
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // GET: Cars/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> Create(Car car)
        {
            if (ModelState.IsValid)
            {
                await car.SaveAsync();
                return RedirectToAction("Index");
            }

            return View(car);
        }

        // GET: Cars/Edit/5
        public async System.Threading.Tasks.Task<ActionResult> Edit(String id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = (await new ParseQuery<Car>().WhereEqualTo("objectId", id).FirstAsync()); ;
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // POST: Cars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> Edit(Car car)
        {
            if (ModelState.IsValid)
            {
                await car.SaveAsync();
                return RedirectToAction("Index");
            }
            return View(car);
        }

        // GET: Cars/Delete/5
        public async System.Threading.Tasks.Task<ActionResult> Delete(String id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = (await new ParseQuery<Car>().WhereEqualTo("objectId", id).FirstAsync());
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // POST: Cars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> DeleteConfirmed(String id)
        {
            Car car = (await new ParseQuery<Car>().WhereEqualTo("objectId", id).FirstAsync());
            await car.DeleteAsync();
            return RedirectToAction("Index");
        }
        
    }
}
