﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AlisProject.Models
{
    public class db : DbContext
    {
        DbSet<Driver> drivers { get; set; }
        DbSet<Car> cars { get; set; }
        DbSet<Shipment> shipments { get; set; }

        public System.Data.Entity.DbSet<AlisProject.Models.Car> Cars { get; set; }

        
        public System.Data.Entity.DbSet<AlisProject.Models.Shipment> Shipments { get; set; }

        public System.Data.Entity.DbSet<AlisProject.Models.Driver> Drivers { get; set; }

    }
}