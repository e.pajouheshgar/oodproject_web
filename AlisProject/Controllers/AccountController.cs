﻿using AlisProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlisProject.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(User usr)
        {
            if (usr.Username == "admin" && usr.Password == "admin")
            {
                Session["u"] = usr.Username;
                Session["p"] = usr.Password;
                return RedirectToAction("index", "Drivers");
            }
            return View();
        }
    }
}