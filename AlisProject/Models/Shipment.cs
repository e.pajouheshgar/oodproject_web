﻿using Parse;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace AlisProject.Models
{

    [ParseClassName("Shipment")]
    public class Shipment : ParseObject
    {
        [Display(Name = "وزن بار")]

        [ParseFieldName("weight")]
        public int weight
        {
            get { return GetProperty<int>(); }
            set { SetProperty<int>(value); }
        }
        [Display(Name = "راننده")]

        [ParseFieldName("Driver")]
        public String Driver
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        public  Driver Driver_
        {
            set;get;
        }

        public virtual List<Car> cars { get; set; }

        [Display(Name = "توضیحات")]

        [ParseFieldName("Description")]
        public String Description
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [Display(Name = "نام خریدار")]

        [ParseFieldName("BuyerFirstName")]
        public String BuyerFirstName
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [Display(Name = "نام خانوادگی خریدار")]

        [ParseFieldName("BuyerLastName")]
        public String BuyerLastName
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [Display(Name = "کد ملی خریدار")]

        [ParseFieldName("BuyerNational_Code")]
        public string BuyerNational_Code
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }


        [Display(Name = " طول جغرافیایی مقصد")]

        [ParseFieldName("DestinationLongitude")]
        public double DestinationLongitude
        {
            get { return GetProperty<double>(); }
            set { SetProperty<double>(value); }
        }
        [Display(Name = "عرض جغرافیایی مقصد")]

        [ParseFieldName("DestinationLatitude")]
        public double DestinationLatitude
        {
            get { return GetProperty<double>(); }
            set { SetProperty<double>(value); }
        }

        [Display(Name = "طول جغرافیایی مبدا")]

        [ParseFieldName("OriginLongitude")]
        public double OriginLongitude
        {
            get { return GetProperty<double>(); }
            set { SetProperty<double>(value); }
        }
        [Display(Name = "عرض جغرافیایی مبدا")]

        [ParseFieldName("OriginLatitude")]
        public double OriginLatitude
        {
            get { return GetProperty<double>(); }
            set { SetProperty<double>(value); }
        }
        [Display(Name = "وضعیت")]

        [ParseFieldName("Status")]
        public string Status
        {
            get { return GetProperty<string>(); }
            set { SetProperty<string>(value); }
        }

        [Display(Name = "خودرو ")]
        [ParseFieldName("car")]
        public Car car
        {
            get { return GetProperty<Car>(); }
            set { SetProperty<Car>(value); }
        }
    }
}