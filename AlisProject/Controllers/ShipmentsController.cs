﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AlisProject.Models;
using Parse;

namespace AlisProject.Controllers
{
    public class ShipmentsController : Controller
    {

        // GET: Shipments
        public async System.Threading.Tasks.Task<ActionResult> Index()
        {
            var res = (await new ParseQuery<Shipment>().Include("car")
                .FindAsync()).ToList();
            foreach (var shipment in res)
            {
                try
                {
                    shipment.Driver_ = new Driver(await ParseUser.Query.WhereEqualTo("objectId", shipment.Driver).FirstAsync());
                }
                catch
                {
                    shipment.Driver_ = new Driver() { Username = "-" };
                }
            }
            return View(res);

        }
        public async System.Threading.Tasks.Task<ActionResult> CustomersIndex()
        {
            return View((await new ParseQuery<Shipment>()
                .FindAsync()).ToList());
        }

        // GET: Shipments/Details/5
        public async System.Threading.Tasks.Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shipment Shipment = (await new ParseQuery<Shipment>().Include("Driver").Include("car").WhereEqualTo("objectId", id).FirstAsync());
            if (Shipment == null)
            {
                return HttpNotFound();
            }
            return View(Shipment);
        }

        // GET: Shipments/Create
        public async System.Threading.Tasks.Task<ActionResult> Create()
        {
            var x = (await ParseUser.Query.Include("car").FindAsync()).ToList();
            List<Driver> d = new List<Driver>();
            foreach (var y in x)
            {
                d.Add(new Driver(y));
            }
            Shipment c = new Shipment();
            Utility.Data.drivers = d;
            c.cars = (await new ParseQuery<Car>().FindAsync()).ToList();
            return View(c);
        }

        // POST: Shipments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> Create(Shipment Shipment)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Shipment.Driver = new Driver(await ParseUser.Query.WhereEqualTo("objectId", Shipment.Driver.ObjectId).FirstAsync());
                    //await ParseUser.LogInAsync(Shipment.Driver.Username, ((Driver)Shipment.Driver).VPassword);
                    await Shipment.SaveAsync();
                }
                catch (Exception er)
                {
                    int y = 0;
                }

                try
                {
                    Shipment.Driver_ = new Driver(await ParseUser.Query.WhereEqualTo("objectId", Shipment.Driver).FirstAsync());

                    if (Shipment.Status.Equals("Initiated"))
                    {
                        ((Driver)Shipment.Driver_).Status = "در ماموریت";
                        await Shipment.Driver_.SaveAsync();
                    }
                    if (Shipment.Status.Equals("Delivered"))
                    {
                        ((Driver)Shipment.Driver_).Status = "آزاد";
                        await Shipment.Driver_.SaveAsync();
                    }
                }
                catch { }
                return RedirectToAction("Index");
            }

            return View(Shipment);
        }

        // GET: Shipments/Edit/5
        public async System.Threading.Tasks.Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shipment Shipment = (await new ParseQuery<Shipment>().Include("car").Include("Driver").Include("Buyer")
                .WhereEqualTo("objectId", id).FirstAsync());
            if (Shipment == null)
            {
                return HttpNotFound();
            }

            var x = (await ParseUser.Query.Include("car").FindAsync()).ToList();
            List<Driver> d = new List<Driver>();
            foreach (var y in x)
            {
                d.Add(new Driver(y));
            }
            Utility.Data.drivers = d;
            return View(Shipment);
        }

        // POST: Shipments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Shipment Shipment)
        {
            if (ModelState.IsValid)
            {
                await Shipment.SaveAsync();
                try
                {
                    Shipment.Driver_ = new Driver(await ParseUser.Query.WhereEqualTo("objectId", Shipment.Driver).FirstAsync());

                    if (Shipment.Status.Equals("Initiated"))
                    {
                        ((Driver)Shipment.Driver_).Status = "در ماموریت";
                        await Shipment.Driver_.SaveAsync();
                    }
                    if (Shipment.Status.Equals("Delivered"))
                    {
                        ((Driver)Shipment.Driver_).Status = "آزاد";
                        await Shipment.Driver_.SaveAsync();
                    }
                }
                catch { }
                return RedirectToAction("Index");
            }
            return View(Shipment);
        }

        // GET: Shipments/Delete/5
        public async System.Threading.Tasks.Task<ActionResult> Delete(String id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shipment Shipment = (await new ParseQuery<Shipment>().WhereEqualTo("objectId", id).FirstAsync());
            if (Shipment == null)
            {
                return HttpNotFound();
            }
            return View(Shipment);
        }

        // POST: Shipments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> DeleteConfirmed(string id)
        {
            Shipment Shipment = (await new ParseQuery<Shipment>().WhereEqualTo("objectId", id).FirstAsync());
            await Shipment.DeleteAsync();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> customer(string id, int s = 0)
        {
            try
            {
                var shipment = await new ParseQuery<Shipment>().WhereEqualTo("objectId", id).FirstAsync();
                if (s == 6)
                {
                    shipment.Status = "Received";
                    await shipment.SaveAsync();
                }
                ParseQuery<ShipmentLocations> qry = new ParseQuery<ShipmentLocations>()
                    .Include("Driver").WhereEqualTo("Shipment", shipment);
                var data = (await qry.FindAsync()).ToList();
                if (shipment!=null && s > 0 && s <= 5  )
                {
                    try
                    {
                        var driver =new Driver( await  ParseUser.Query.WhereEqualTo("objectId", shipment.Driver).FirstAsync());

                        await ParseUser.LogInAsync(driver.Username, driver.VPassword);
                        double s1 = driver.Get<double>("score");
                        double s2 = driver.scoreCount;
                        driver["score"] = (s1 * s2 + s) / (s2 + 1);
                        driver["Status"] = "آزاد";
                        driver.Increment("scoreCount");
                        await driver.getUse().SaveAsync();
                        try
                        {
                            ParseUser.LogOut();
                        }
                        catch { }
                    }
                    catch { }
                    if (shipment != null)
                    {
                        shipment.Status = "Rated";
                        await shipment.SaveAsync();
                    }
                }

                return View(new ViewModels.ShipmentLocationsDetail()
                {
                    data = data,
                    shipment = shipment
                });
            }
            catch { }
            return View(new List<Shipment>());
        }
    }
}
