﻿using Parse;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AlisProject.Models
{
    [ParseClassName("User")]
    public class Driver : ParseUser
    {
        

        public Driver() { }
        public Driver(ParseUser parseUser)
        {
            ObjectId = parseUser.ObjectId;
            Username = parseUser.Username;
            if (parseUser.ContainsKey("VPassword"))
                VPassword = parseUser.Get<String>("VPassword");
            if (parseUser.ContainsKey("First_Name"))
                First_Name = parseUser.Get<String>("First_Name");
            if (parseUser.ContainsKey("Last_Name"))
                Last_Name = parseUser.Get<String>("Last_Name");
            if (parseUser.ContainsKey("National_Code"))
                National_Code = parseUser.Get<String>("National_Code");
            if (parseUser.ContainsKey("License_Number"))
                License_Number = parseUser.Get<String>("License_Number");
            if (parseUser.ContainsKey("car"))
                car = parseUser.Get<Car>("car");
            if (parseUser.ContainsKey("Status"))
                Status = parseUser.Get<String>("Status");
            if (parseUser.ContainsKey("LastLocation"))
                LastLocation = parseUser.Get<String>("LastLocation");
            if (parseUser.ContainsKey("score"))
                score = parseUser.Get<double>("score");
            if (parseUser.ContainsKey("scoreCount"))
                score = parseUser.Get<double>("scoreCount");
            if (parseUser.ContainsKey("LastLocationTime"))
                LastLocationTime = parseUser.Get<DateTime?>("LastLocationTime");

        }
        public ParseUser getUse()
        {
            ParseUser p = new ParseUser();

            p.ObjectId = this.ObjectId;
            p.Username = this.Username;
            p.Password = this.VPassword;

            p["VPassword"] = this.VPassword;
            p["First_Name"] = First_Name;
            p["Last_Name"] = Last_Name;
            p["National_Code"] = National_Code;
            p["License_Number"] = License_Number;
            p["car"] = car;
            p["Status"] = Status;
            p["score"] = score;
            p["LastLocation"] = LastLocation;
            p["LastLocationTime"] = LastLocationTime;
            p["scoreCount"] = scoreCount;


            return p;
        }

        [Display(Name = "کلمه عبور")]

        [ParseFieldName("VPassword")]
        public string VPassword
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [Display(Name = "نام")]

        [ParseFieldName("First_Name")]
        public string First_Name
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [Display(Name = "نام خانوادگی")]

        [ParseFieldName("Last_Name")]
        public string Last_Name
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [Display(Name = "کد ملی")]

        [ParseFieldName("National_Code")]
        public string National_Code
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [Display(Name = "شماره گواهینامه")]

        [ParseFieldName("License_Number")]
        public string License_Number
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [Display(Name = "وضعیت")]

        [ParseFieldName("Status")]
        public string Status
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [Display(Name = "آخرین مکان")]

        [ParseFieldName("LastLocation")]
        public string LastLocation
        {
            get { return GetProperty<String>(); }
            set { SetProperty<String>(value); }
        }
        [Display(Name = "زمان آخرین مکان")]

        [ParseFieldName("LastLocationTime")]
        public DateTime? LastLocationTime
        {
            get { return GetProperty<DateTime?>(); }
            set { SetProperty<DateTime?>(value); }
        }
        [Display(Name = "خودرو در دست راننده")]
        [ParseFieldName("car")]
        public Car car
        {
            get { return GetProperty<Car>(); }
            set { SetProperty<Car>(value); }
        }

        [Display(Name = "امتیاز")]
        [ParseFieldName("score")]
        public double score
        {

            get { return GetProperty<double>(); }
            set { SetProperty<double>(value); }
        }
        [Display(Name = "امتیاز")]
        [ParseFieldName("scoreCount")]
        public double scoreCount
        {

            get { return GetProperty<double>(); }
            set { SetProperty<double>(value); }
        }
    }
}